void main()
{
  // Apply Fogbreath VFX
  object oPC = GetFirstPC();
  effect eFogBref = EffectVisualEffect( 780, FALSE );
  eFogBref = ExtraordinaryEffect(eFogBref);
  ApplyEffectToObject(DURATION_TYPE_PERMANENT, eFogBref, oPC);
}
